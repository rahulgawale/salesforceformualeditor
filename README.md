# SalesforceFormualEditor

Formatting tool for Salesforce formula.

It is very common that long formulas in salesforce are very hard to read in the standard salesforce UI, we often need to copy and paste it in editor and indent it manually.
I am building a single page web app to format and manage salesforce formula. Feel free to fork and suggest any changes.

